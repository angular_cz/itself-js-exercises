var baseConfig = require('../../base-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);

  config.set({
    basePath: '../../'
  });

  config.files.push('bower_components/react/react-with-addons.js');
  config.files.push('bower_components/react/react-dom.js');

  config.files.push('09-react/transpiled/complete/calculations/operations.js');

  config.files.push('09-react/transpiled/complete/components/button.js');
  config.files.push('09-react/transpiled/complete/components/operation.js');
  config.files.push('09-react/transpiled/complete/components/display.js');

  config.files.push('09-react/transpiled/complete/components/buttons.js');
  config.files.push('09-react/transpiled/complete/components/calculator.js');

  config.files.push('09-react/transpiled/complete/components/display.spec.js');
  config.files.push('09-react/transpiled/complete/components/calculator.spec.js');

};
