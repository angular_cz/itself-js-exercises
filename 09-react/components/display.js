import React from 'React';

let Display = (props) => {      // TODO 1.1 -  definujte display - input pouze pro čtení
  return <div className="display">
    // zde bude input
  </div>

};

// TODO 1.2 -  přiřaďte definici typů propTypes jako vlastnost funkce Display

let propTypes = {
  value: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
  ]).isRequired
};

export default Display
