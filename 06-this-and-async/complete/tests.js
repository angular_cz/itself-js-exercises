import {Countdown} from './index'

describe('Odpočet', function() {

  it('nastavuje intervalID', function() {
    const countdown = new Countdown(1);
    countdown.start(function() {
    });

    expect(countdown.intervalID).not.toBeNull();
  });

  describe("callback", function() {

    // TODO 1.3 - Zrychlete testy
    beforeEach(function() {
      jasmine.clock().install();
    });

    afterEach(function() {
      jasmine.clock().uninstall();
    });

    it('update volá po vteřině (TODO 1.1) (TODO 1.2)', function(done) {

      const countdown = new Countdown(1);

      const updateCallback = function(value) {
        // TODO 1.1 - Ověřte snížení hodnoty
        expect(value).toBe(0);
        done();
      };

      countdown.start(updateCallback);

      // TODO 1.3 - Zrychlete test - Poskočte o 1001
      jasmine.clock().tick(1001);
    });

    it('final volá po skončení odpočtu (TODO 1.3)', function(done) {

      const countdown = new Countdown(3);

      countdown.start(null, function(value) {
        expect(value).toBe(3);
        done();
      });

      // TODO 1.3 - Zrychlete testy - Poskočte o 4000
      jasmine.clock().tick(4000);
    });

    it('final nevolá, pokud byl odpočet zastaven (TODO 1.5)', function(done) {

      const countdown = new Countdown(3);

      countdown.start(null, function() {
        // TODO 1.4 - finalCallback nemá být volán po zastavení odpočtu metodou stop
        done.fail("final callback byl zavolán i když byl odpočet zastaven");
      });

      expect(countdown.isRunning()).toBe(true);

      countdown.stop();

      jasmine.clock().tick(4000);
      done();
    });

  });

});
