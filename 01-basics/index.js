// ## 1. Undefined / null ------------------------------------------------------------------------- ##

{
// 1.1 - Proměnná "defined" nesmí být undefined
  let defined;

  console.assert(typeof defined != 'undefined', '1.1 - Proměnná "defined" nesmí být undefined');

// 1.2 - Proměnná "nullVariable" musí být skutečně null
  const nullVariable = 'not-null';

  console.assert(!nullVariable && typeof nullVariable == 'object', '1.2 - Proměnná "nullVariable" musí být skutečně null');

// 1.3 - Rozdíl mezi let a const - odkomentujte řádek 21, všimněte si chyby a opravte ji
  const randomNumber = Math.random();  // nemodifikujte

  const increment = randomNumber;

  console.assert(increment === randomNumber, '1.3 - Nemodifikujte iniciální obsah proměnné increment');

  // increment++;   // odkomentujte řádek, všimněte si chyby a opravte ji, nemodifikujte testy
  console.assert(increment > randomNumber, '1.3 - Proměnná increment by měla být modifikovatelná');
}

// ## 2. Boolean ---------------------------------------------------------------------------------- ##
{
  const notBoolean = 'do-not-modify';  // Nemodifikujte

// 2.1 - Upravte explicitBoolean tak, aby byla typu boolean
  const explicitBoolean = notBoolean;

  console.assert(notBoolean === 'do-not-modify', '2.1 - Proměnnou "notBoolean" nemodifikujte');
  console.assert(typeof explicitBoolean == 'boolean', '2.1 - Proměnná "explicitBoolean" musí být opravdu boolean');

// 2.2 - Upravte výrazy tak, aby byly vyhodnoceny na false
  const stringToBoolean = 'non-empty';
  const numberToBoolean = 42;
  const objectToBoolean = {};

  console.assert(typeof stringToBoolean == 'string', '2.2.1 - Proměnná "stringToBoolean" musí být typu string');
  console.assert(!Boolean(stringToBoolean), '2.2.1 - Proměnná "stringToBoolean" musí být vyhodnocena na false');

  console.assert(typeof numberToBoolean == 'number', '2.2.2 - Proměnná "numberToBoolean" musí být typu číslo');
  console.assert(!Boolean(numberToBoolean), '2.2.2 - Proměnná "numberToBoolean" musí být vyhodnocena na false');

  console.assert(typeof objectToBoolean == 'object', '2.2.3 - Proměnná "objectToBoolean" musí být typu object');
  console.assert(!Boolean(objectToBoolean), '2.2.3 - Proměnná "objectToBoolean" musí být vyhodnocena na false');

}

// ## 3. String ----------------------------------------------------------------------------------- ##
{
  const randomNumber = Math.random();  // nemodifikujte

// 3.1 - Převeďte číslo na řetězec
  const randomString = randomNumber;

  console.assert(randomString == randomNumber, '3.1 - Při vyhodnocení používejte "randomNumber"');
  console.assert(typeof randomString == "string", '3.1 - Proměnná "randomString" musí být řetězcem');

// 3.2 - Přiřaďte do proměnné délku řetězce randomString
  const stringLength = 0;

  console.assert(stringLength === randomString["\x6C\x65\x6E\x67\x74\x68"], '3.2 - Proměnná "stringLength" musí obsahovat délku řetězce "randomString"');

  const firstString = 'Ahoj náhodné čislo:';  // Nemodifikujte
  const secondString = randomString;          // Nemodifikujte

// 3.3 - Spojte řetězce firstString a secondString
  const concatenatedString = "";

  console.assert(concatenatedString.indexOf(firstString) === 0 && concatenatedString.indexOf(secondString) > -1 && firstString["\x6C\x65\x6E\x67\x74\x68"] + secondString["\x6C\x65\x6E\x67\x74\x68"] == concatenatedString["\x6C\x65\x6E\x67\x74\x68"], '3.3 - Proměnná "concatenatedString", musí být spojením řetězců "firstString" a "secondString"')

// 3.4 - Použijte syntaxi string literalu, rozdělte na víc řádků a vložte do něj proměnnou bez použití operátoru zřetězení
  const insertedVariable = randomString;  // Nemodifikujte

  const templateLiteralString = 'Víceřádkový řetězec, zde zalomte a sem vložte >>insertedVariable<<';

  console.assert(templateLiteralString.indexOf('\n') !== -1 && templateLiteralString.indexOf(insertedVariable) !== -1, '3.4 - Proměnná "templateLiteralString", má obsahovat víceřádkový řetězec a proměnnou"')
}

// ## 4. Čísla ------------------------------------------------------------------------------------ ##
{
// 4.1 - Změnou operandu vytvořte nekonečno
  const divisor = 1;

  const infinityNumber = 10 / divisor;  // nemodifikujte

  console.assert(!Number.isFinite(infinityNumber), '4.1 - Proměnná "infinityNumber" musí být nekonečno');

// 4.2 - Opravte operand aby nebyl výpočet NaN
  const operand1 = 'number one';

  const operand2 = 1; // nemodifikujte
  const result = operand1 - operand2;  // nemodifikujte

  console.assert(!Number.isNaN(result), '4.2 - Proměnná "result" nesmí mít hodnotu NaN');

  const stringOperand = "1"; // nemodifikujte

// 4.3 - Upravte použití stringOperandu, tak  aby byl výsledkem součet
  const numberResult = 1 + stringOperand;

  console.assert(numberResult === 2, '4.3 - Hodnota proměnné "numberResult" musí být součet');

  const decimal1 = 0.2;   // nemodifikujte
  const decimal2 = 0.1;   // nemodifikujte
  const decimalSum = decimal1 + decimal2;  // nemodifikujte

// 4.4 - Upravte porovnání tak, aby byl výsledek true
  const decimalComparison = (decimalSum == 0.3);

  console.assert(decimalComparison, '4.4 - Výsledkem porovnání "decimalComparison" musí být true');

  const decimalNumber = 3.14156; // nemodifikujte

// 4.5 - Ořízněte na dvě desetinná místa pomocí metody na number;
  const truncatedDecimal = decimalNumber;

  console.assert(truncatedDecimal === "3.14", '4.5 - Proměnná "truncatedDecimal" musí obsahovat oříznuté číslo "decimalNumber"');
}

// ## 5. Math ------------------------------------------------------------------------------------- ##
{
  const decimalNumberExact = Math.PI; // nemodifikujte

// 5.1 - Zaokrouhlete pomocí objektu Math nahoru.
  const ceiledNumber = decimalNumberExact;

  console.assert(ceiledNumber === 4, '5.1 - Proměnná "ceiledNumber" obsahovat "decimalNumberExact" zaokrouhlený nahoru');
}

// ## 6. Porovnání -------------------------------------------------------------------------------- ##
{
  const stringOperand = "42";  // nemodifikujte
  const numberOperand = 42;    // nemodifikujte

// 6.1 - Porovnejte operandy stringOperand a numberOperand typově - musí vrátit false
  const typeComparison = true;

  console.assert(typeComparison === false, '6.1 - Proměnná "typeComparison" musí obsahovat typové porovnání "stringOperand" a "numberOperand"');

// 6.2 - Porovnejte operandy stringOperand a numberOperand netypově - musí vrátit true
  const nonTypeComparison = false;

  console.assert(nonTypeComparison === true, '6.2 - Proměnná "nonTypeComparison" musí obsahovat netypové porovnání "stringOperand" a "numberOperand"');

}

// ## 7. Operátory konjunkce, dizjunkce ----------------------------------------------------------- ##
{

// 7.1 - Modifikujte operand, tak aby byl v následujícím porovnáním vrácen poslední operand
// const orResult = operand1 || operand2 || lastOperand;
  const operand1 = true;
  const operand2 = false;

  const lastOperand = Math.random();  // nemodifikujte
  const orResult = operand1 || operand2 || lastOperand; // nemodifikujte

  console.assert(orResult === lastOperand, '7.1 - Proměnná "orResult" musí obsahovat poslední operand');

// 7.2 Modifikujte operand, tak aby se assert nevykonal
  const operand = true;

  const andResult = operand && console.assert(false, '7.2 - tento assert vždy selže, zajistěte, aby se nevolal'); // nemodifikujte

}
