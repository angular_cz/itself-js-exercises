"use strict";

const gulp = require('gulp');
const babel = require('gulp-babel');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');

const courseware = require('courseware');

const fs = require('fs');
const path = require('path');

const config = {
  httpServer: {
    host: 'localhost',
    port: 8000
  }
};

let courseWareTestResultsSocket;

gulp.task('server', ['initialize-tests'], () => {
  const server = require('./server.js');
  courseWareTestResultsSocket = server(config);
});

const transpileES6 = (filePath, exerciseDir) => {

  exerciseDir = exerciseDir || filePath.split(path.sep)[0];

  const processErrorToFile = (error) => {
    let dirname = path.dirname(error.fileName);
    if (exerciseDir) {
      const index = dirname.indexOf(exerciseDir) + exerciseDir.length;
      dirname = dirname.substring(0, index) + '/transpiled' + dirname.substring(index) + '/';
    } else {
      dirname += '/transpiled/';
    }

    const file = dirname +
      path.basename(error.fileName, path.extname(error.fileName)) +
      '.js';

    const content = '__processError(' + JSON.stringify(error) + ');';
    fs.writeFile(file, content);

    console.error(error.name, error.message);
    console.log(error.codeFrame);
  };

  return gulp.src(filePath, {base: exerciseDir})
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015', 'react'],
      plugins: [
        'transform-es2015-modules-umd',
        ["babel-plugin-transform-builtin-extend", {
          globals: ["Error", "Array"]
        }]
      ]
    }).on('error', processErrorToFile))
    .pipe(sourcemaps.write(null, {
      mapSources: function(sourcePath) {
        return '/' + exerciseDir + '/' + sourcePath;
      }
    }))
    .pipe(gulp.dest('./' + exerciseDir + '/transpiled'));
};

gulp.task('default', ['server'], function() {

  gulp.watch(['[0-9][0-9]*/**/*.js', '[0-9][0-9]*/**/*.jsx',
    '!**/transpiled/**'], (event) => {
    const relativePath = path.relative(__dirname, event.path);
    console.log('File changed:', relativePath);

    transpileES6(relativePath);
  });


  gulp.watch(['[0-9][0-9]*/**/*.html', '[0-9][0-9]*/transpiled/**/*.js'], (event) => {
    const relativePath = path.relative(__dirname, event.path);
    courseWareTestResultsSocket.notify('reload', {file: relativePath});
    console.log('Livereload: ', relativePath);
  });
});

gulp.task('es6-init', function(done) {
  function getExercisesFolders(dir) {
    return fs.readdirSync(dir)
      .filter(function(file) {
        return fs.statSync(path.join(dir, file)).isDirectory();
      }).filter(function(file) {
        return /[0-9][0-9].*/.test(file);
      })
  }

  const partDone = function(file) {
    return function() {
      if (--remainingCount < 1) {
        done();
      }
    }
  };

  console.log("Js-exercises - ES6 initial transpilation...");

  const folders = getExercisesFolders('./');
  let remainingCount = folders.length;

  folders.map(function(file) {
    const exerciseDir = file.split(path.sep)[0];
    const src = [
      exerciseDir + '/**/*.js',
      exerciseDir + '/**/*.jsx',
      '!' + exerciseDir + '/transpiled/**',
    ];

    transpileES6(src, exerciseDir)
      .on('end', partDone(file));
  });

});

gulp.task('initialize-tests', ['es6-init'], function(cb) {
  const currentKarmaServer = require("karma").Server;
  courseware.initializeTestResults(currentKarmaServer, cb);
});
