var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });

  config.files.push('04-oop/transpiled/calculator.js');
  config.files.push('04-oop/transpiled/calculator.spec.js');
  config.files.push('04-oop/transpiled/generator-oop.js');
  config.files.push('04-oop/transpiled/generator-oop.spec.js');

};

