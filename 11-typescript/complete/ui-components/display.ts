export class Display {
  private isResult_ = false;
  private value_: string = '';

  constructor(private element_: HTMLElement) {
    this.clear();
  }

  setValue(value: string) {
    this.isResult_ = true;
    this.value_ = value;
    this.render_();
  }

  addToValue(value: string) {
    if (this.isResult_) {
      this.setValue("");
      this.isResult_ = false;
    }

    this.value_ += value;
    this.render_();
  }

  getValue() {
    return this.value_;
  }

  clear() {
    this.setValue("0");
  }

  render_() {
    this.element_.setAttribute("value", this.value_);
  }
}
